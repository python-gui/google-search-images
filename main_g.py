from PyQt5 import QtWidgets, uic
import sys


from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QFileDialog

from PyQt5.QtCore import pyqtSlot
import sys

from PyQt5.QtGui import *
import pandas
from google_img import Worker

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import json
from configparser import ConfigParser




data = {'col1':['1','2','3','4'],
        'col2':['1','2','1','3'],
        'col3':['1','1','2','1']}


class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        self.threadpool = QThreadPool()

        uic.loadUi('g_searche_advanced.ui', self) # Load the .ui file
        self.show() # Show the GUI



        # self.tbl.setRowCount(1) 
  
        #Column count
        self.tbl.setColumnCount(5)  

        self.tbl.setHorizontalHeaderLabels(['Keyword', "Size","widthXheight",'Image Url',"Local Path"])

        

        # self.table.horizontalHeader().sectionDoubleClicked.connect(self.changeHorizontalHeader)


  
        # self.tbl.setItem(0,0, QTableWidgetItem("Name"))
        # self.tbl.setItem(0,1, QTableWidgetItem("City"))
        # self.tbl.setItem(1,0, QTableWidgetItem("Aloysius"))
        # self.tbl.setItem(1,1, QTableWidgetItem("Indore"))
        # self.tbl.setItem(2,0, QTableWidgetItem("Alan"))
        # self.tbl.setItem(2,1, QTableWidgetItem("Bhopal"))
        # self.tbl.setItem(3,0, QTableWidgetItem("Arnavi"))
        # self.tbl.setItem(3,1, QTableWidgetItem("Mandsaur"))
        
        self.search_btn.clicked.connect(self.google_img)
        # self.img..
        self.im = QPixmap("./1.webp")
        
        self.img.setPixmap(self.im)

   

        # rowPosition = self.tbl.rowCount()
        # print("==========",rowPosition)
        # self.tbl.insertRow(rowPosition)

        # self.tbl.setItem(rowPosition , 0, QTableWidgetItem("text1"))
        # self.tbl.setItem(rowPosition , 1, QTableWidgetItem("text2"))
        # self.tbl.setItem(rowPosition , 2, QTableWidgetItem("text3"))
                # self.tbl.insertRow(rowPosition)




        self.export_excel.clicked.connect(self.excel_save)
        self.export_csv.clicked.connect(self.csv_save)
        self.export_json.clicked.connect(self.json_save)
        
        self.tbl.itemSelectionChanged.connect(self.cell_was_selected)
        
        # self.apply_filter.clicked.connect(self.get_filter)
        
        # self.apply_filter.clicked.connect(self.get_apply)
        
        # self.set_config()


    # def action_(self):
    #     print('clicked')
    
    def cell_was_clicked(self, row, column):
        print("Row %d and Column %d was clicked" % (row, column))
        # item = self.tbl.ite.mAt(row, column)
        # self.ID = tbl.text()
        
    def cell_was_selected(self):
        currentrow = self.tbl.currentRow()
        print(self.tbl.item(currentrow, 1).text())

        
    def set_config(self):
        print("a")
        config = ConfigParser()
        config.read('config.ini')
        # print(config["min resolution"]["height"])
        
        self.min_r_w.setText(str(config["min resolution"]["height"]))
        self.min_r_h.setText(str(config["min resolution"]["width"]))
        
        self.max_r_w.setText(str(config["max resolution"]["height"]))
        self.max_r_h.setText(str(config["max resolution"]["width"]))
        
        self.ratio_from.setText(str(config["ratio"]["from"]))
        self.ratio_to.setText(str(config["ratio"]["to"]))

        
        # config.set('min resolution', "width", ())
        
        # config.set('max resolution', "height", self.max_r_w.text())
        # config.set('max resolution', "width", self.max_r_h.text())
        
        # config.set('ratio', "from", self.ratio_from.text())
        # config.set('ratio', "to", self.ratio_to.text())   
        
        
        
    
    def get_filter(self):
        filter_={"Filter":
             {
                 "min resolution":{
                     "height":self.min_r_h.text(),
                     "width":self.min_r_w.text(),
                     },
                 "max resolution":{
                     "height":self.max_r_w.text(),
                     "width":self.max_r_h.text()
                         },
                 "ratio":{
                     "from":self.ratio_from.text(),
                     "to":self.ratio_to.text(),
                     }
                }
         }
            
        config = ConfigParser()
        config.read('config.ini')
        
        try:
            config.add_section('filter')
        except:
            pass
        
        try:
            config.add_section('logo')
        except:
            pass
        
        try:
            config.add_section('min resolution')
        except:
            pass
        try:

            config.add_section('max resolution')
        except:
            pass
        try:

            config.add_section('ratio')
        except:
            pass

        
        config.set('min resolution', "height", self.min_r_h.text())
        config.set('min resolution', "width", self.min_r_w.text())
        
        config.set('max resolution', "height", self.max_r_w.text())
        config.set('max resolution', "width", self.max_r_h.text())
        
        config.set('ratio', "from", self.ratio_from.text())
        config.set('ratio', "to", self.ratio_to.text())        
        
        
        
        with open('config.ini', 'w') as f:
            config.write(f)
            
        # out_file = open("config.json", "w+")
        
        # print("=====",out_file.read())
        
        # # js1=json.loads(out_file.read())
        
        # js1["filter"]=filter_
        
        # json.dump(dict1, out_file, indent = 6)
  
        # out_file.close()
            
        # f = open("config.txt", "w")
        # print(f.read())
        
        
        
        # f = open("demofile2.txt", "a")
        # f.write("Now the file has more content!")
        # f.close()

        # print(filter_)
        
        
        
    def get_apply(self):
            
            filter_={"Filter":
                 {
                     "min resolution":{
                         "height":self.min_r_h.text(),
                         "width":self.min_r_w.text(),
                         },
                     "max resolution":{
                         "height":self.max_r_w.text(),
                         "width":self.max_r_h.text()
                             },
                     "ratio":{
                         "from":self.ratio_from.text(),
                         "to":self.ratio_to.text(),
                         }
                    }
             }
                
            config = ConfigParser()
            config.read('config.ini')
            
            try:
                config.add_section('logo')
            except:
                pass
            


            print("=========", str(self.trans.value()))
            config.set('logo', "trans", str(self.trans.value()))
            
            # checkboxs=["left_top","right_top","center","left_buttom","right_buttom"]
            
            

            if self.left_top.isChecked():
                config.set('logo', "postion", self.left_top.text())

                # print(self.left_top.text())
            elif self.right_top.isChecked():
                config.set('logo', "position", self.right_top.text())

                # print(self.right_top.text())
                
                
            elif self.center.isChecked():
                config.set('logo', "postion", self.center.text())

                # print(self.center.text())
            elif self.left_buttom.isChecked():
                config.set('logo', "postion", self.left_buttom.text())
                
                # print(self.left_buttom.text())
            elif self.right_buttom.isChecked():
                config.set('logo', "postion", self.right_buttom.text())

                # print(self.right_buttom.text())
                
            
                    
                    
                    
                    
            config.set('logo', "margin top", str(self.margin_top.text()))
            config.set('logo', "margin buttom", str(self.margin_buttom.text()))
            config.set('logo', "margin left", str(self.margin_left.text()))
            config.set('logo', "margin right", str(self.margin_right.text()))


            
            # config.set('max resolution', "height", self.max_r_w.text())
            # config.set('max resolution', "width", self.max_r_h.text())
            
            # config.set('ratio', "from", self.ratio_from.text())
            # config.set('ratio', "to", self.ratio_to.text())        
                
                
                
            with open('config.ini', 'w') as f:
                config.write(f)
                

            
        
    def print_output(self, s):
        print(s)
        rowPosition = self.tbl.rowCount()
        self.tbl.insertRow(rowPosition)
        
        self.tbl.setItem(rowPosition , 0, QTableWidgetItem(s["keyword"]))
        self.tbl.setItem(rowPosition , 1, QTableWidgetItem(s["img url"]))
        


    def setData(self): 
        horHeaders = []
        for n, key in enumerate(sorted(self.data.keys())):
            horHeaders.append(key)
            for m, item in enumerate(self.data[key]):
                newitem = QTableWidgetItem(item)
                self.setItem(m, n, newitem)
        self.setHorizontalHeaderLabels(horHeaders)


    @staticmethod
    def write_qtable_to_df(table):
        col_count = table.columnCount()
        row_count = table.rowCount()
        headers = [str(table.horizontalHeaderItem(i).text()) for i in range(col_count)]

        # df indexing is slow, so use lists
        df_list = []
        for row in range(row_count):
            df_list2 = []
            for col in range(col_count):

                table_item = table.item(row,col)
                df_list2.append('' if table_item is None else str(table_item.text()))
            df_list.append(df_list2)

        df = pandas.DataFrame(df_list, columns=headers)
        print(df)

        return df
    
    def google_img(self):
        keywords= self.SearchText.text()
        print(keywords)
        
        worker = Worker(keywords)
        worker.signals.result.connect(self.print_output)

        self.threadpool.start(worker)
        

    def excel_save(self):
        name = QFileDialog.getSaveFileName(self, 'Save File',"","Excel(*.xlsx) ")
        df=self.write_qtable_to_df(self.tbl)

        df.to_excel(name[0], index = False)

        print(name)

    def csv_save(self):
        name = QFileDialog.getSaveFileName(self, 'Save File',"","CSV(*.csv) ")
        df=self.write_qtable_to_df(self.tbl)

        df.to_csv(name[0], encoding='utf-8', index = False)

        print(name)
    def json_save(self):
        name = QFileDialog.getSaveFileName(self, 'Save File',"","Json(*.json) ")

        df=self.write_qtable_to_df(self.tbl)

        out = df.to_json(orient='records')

        with open(name[0], 'w') as f:
            f.write(out)

        print(name)
        # file = open(name,'w')
        # text = self.textEdit.toPlainText()
        # file.write(text)
        # file.close()
    


app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
window = Ui() # Create an instance of our class
app.exec_() # Start the application